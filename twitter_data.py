# -*- coding: utf-8 -*-

import tweepy
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import json



#read in from config.json
#empy.config.json is tracked in git, but has no data.  Do NOT stage config.json.
#In your local directory, edit empty.config.json with api keys from twitter.com and save as config.json








# If the authentication was successful, you should
# see the name of the account print out



###################################

# tweet listener test

listen_for = 'fitbit'  # listen for tweets with 'fitbit' in them
tweet_stream = []

class StdOutListener(StreamListener):
    """ A listener handles tweets are the received from the stream.
    This is a basic listener that just prints received tweets to stdout.
    """
    def on_data(self, data):
        data_json = json.loads(data)
        print(data_json['text'])
        return True

    def on_error(self, status):
        print(status)

if __name__ == '__main__':
    l = StdOutListener()
    f = open('config.json','r')
    api_params = json.loads(f.read())
    auth = tweepy.OAuthHandler(api_params['consumer_key'], api_params['consumer_secret'])
    auth.secure = True
    auth.set_access_token(api_params['access_token'], api_params['access_token_secret'])

    stream = Stream(auth, l)
    stream.filter(track=[listen_for])
    

